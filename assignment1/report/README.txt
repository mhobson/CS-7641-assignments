# Project 1: Supervised Learning

Matt Hobson
CS7641 - Spring 2019
GTID: mhobson7

## Code

Code for this assignment can be found here:

    https://gitlab.com/mhobson/cs7641-assignment1

## Data sets

The data files are included in the repository. If you prefer to download the
files yourself, there are links to the sourcce UCI pages in the README files
within each data directory.

## Running the project

This was developed under Python 3.7. (You can probably run it with 3.5 or
later.)

If you are familiar with Pipenv, you can use that to set up your virtual
environment. If not, there's a standard requirements file you can use.
