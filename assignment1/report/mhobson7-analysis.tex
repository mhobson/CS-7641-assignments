\documentclass[12pt,journal]{IEEEtran}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{comment}
% Convert qutoes to smart ones (in case any get left in accidentally).
\usepackage [english]{babel}
\usepackage [autostyle, english = american]{csquotes}
\MakeOuterQuote{"}

\usepackage{cite}

\title{Project 1: Supervised Learning}

\author{Matt~Hobson}

\begin{document}

\maketitle

\section{Data Sets}\label{data-sets}

A summary of the primary characteristics of the data sets is presented in table \ref{tbl:data-set-stats}.

\begin{table}[htb]
	\centering
	\begin{tabular}{lcc}
		\hline
		Data set 					& QSAR 			& Wine 			\\
		\hline
		
		Records 					& 1055 			& 1599 			\\
		Features 					& 41 			& 11 			\\
		Class distribution (\%) 	& 66.3, 33.7 	& 46.5, 53.5 	\\
		Sparse 						& No 			& No 			\\
		Binary 						& Yes 			& Yes 			\\
		Balanced\cite{239982} 		& Yes 			& Yes 			\\
		\hline
	\end{tabular}
	\caption{Data Set Statistics}
	\label{tbl:data-set-stats}
\end{table}

\subsection{QSAR Biodegradation\cite{qsar-data}}\label{data-sets:qsar}

The task of this data set is to attempt to find one or more relationships between a chemical's structure and its biodegradability. These relationships were used to develop QSAR models.

The data set had no missing data, and all features were numeric and continuous. However, as there appeared to be some ordering of the data, the entire set was shuffled five times as a preprocessing step. Also, the final class was comprised of two text values: ``RB'' (ready biodegradable) and ``NRB'' (not ready biodegradable). This column was converted to ``1'' for ``RB'' and 0 otherwise.

The final data set had 1055 rows, with 41 features and a binary class with a somewhat balanced 2:1 ratio.

\subsection{Wine Quality\cite{wine-data}}\label{data-sets:wine}

The task of this data set is to model wine quality based on different objective, measurable features. The wine is called \emph{vinho verde} and comes from northwest Portugal. There were two data sets available; I chose the red wine data for no other reason than that is the type of wine I prefer.

The features of this data set were all continuous numeric values of various ranges. There was little discussion as to how the final class ``quality'' was measured, but the available descriptions suggest that this was a subjective rating of one or more human tasters who graded the quality on a scale of 1 through 10.

As the final class was a range of integers, it was converted to a binary class by using a threshold of the median value five; values of five and below became ``0'', and above five became ``1''. No other preprocessing was performed.

The final data had 1599 rows with 11 features. The binary class was quite balanced with a nearly 1:1 ratio.

\subsection{Methodology}\label{data-sets:preprocessing}

Both data sets had all features standardized using the \texttt{StandardScaler} from the ``scikit-learn'' library\cite{scikit-learn}. This was done because certain algorithms are more sensitive to differences in data ranges than others. While this might provide additional criteria to compare the algorithms, placing algorithms at an obvious disadvantage from the start seemed to go against the intent of the project, which is to compare the algorithms in an apples-to-apples fashion.

The only other preprocessing performed was discussed above (converting the $y$-values to zeros and ones).

\subsection{Why the Data Is ``Interesting''}\label{data-sets:why-interesting}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth,keepaspectratio]{images/data_winequality_histogram.png}
	\caption{Histogram of features present in ``wine quality'' data set.}
	\label{fig:data-wine-histogram}
\end{figure}

The wine data is comprised solely of continuous, bounded features. As shown in figure \ref{fig:data-wine-histogram}, the features present several different types of distributions which may prove interesting.

The QSAR data is interesting because it has significantly more features to work with. This will hopefully provide some interesting data points to compare the algorithms.

A further potential difference is in the class balance. While both sets are considered to be balanced, it is hoped that the difference in ratios is enough to allow some algorithms to distinguish themselves from others.

\pagebreak[3]
\section{Decision Trees}\label{performance:decision-trees}

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table}[htb]
	\centering
	\begin{tabular}{llll}
		\hline
		\multirow{2}{*}{Hyperparameter} 	& \multirow{2}{*}{Values Searched} 	& \multicolumn{2}{l}{Values Chosen} 	\\
																				\cline{3-4} 
											&									& QSAR 				& Wine 				\\
		\hline
		Maximum depth 						& 1, 2, ..., 50 					& 9 				& 14 				\\
		Split functions 					& Gini, entropy 					& Entropy 			& Entropy 			\\
		Class weight 						& Uniform, balanced 				& Balanced 			& Balanced 			\\
		\hline
	\end{tabular}
	\caption{Grid Search for Decision Trees}
	\label{tbl:grid-search:dt}
\end{table}

\begin{figure*}[tbp]
	\centering
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{images/DT_qsar_LC.png}
		\caption{Decision Tree -- QSAR -- Learning Curve}
		\label{fig:dt-qsar-lc}
	\end{minipage}\hspace{.05\linewidth}
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{images/DT_wine_LC.png}
		\caption{Decision Tree -- Wine Quality -- Learning Curve}
		\label{fig:dt-wine-lc}
	\end{minipage}
\end{figure*}

For decision trees, the grid search algorithm performed an exhaustive search of the parameters listed in table \ref{tbl:grid-search:dt}. Maximum depth was used as the pre-pruning method. Both the Gini and entropy splitting methods were considered.

As seen in figure \ref{fig:dt-qsar-lc}, the decision tree algorithm had mixed results. The training data was fitted fairly well by the algorithm, exhibiting a fairly low bias.

The testing data did not fit very well and showed a rather erratic increase in accuracy as more data was used for training. However, it is difficult to tell if this algorithm would continue to improve its accuracy if more data were available, because the erraticism could be interpreted as either still increasing or nearly converged.

One thing is clear -- this data set is a hard problem for decision trees.

Switching to the wine data, figure \ref{fig:dt-wine-lc} shows that the learner fit the training data almost effortlessly. The accuracy of the validation data shows a nearly linear increase in accuracy. Combining that observation with the large gap between the two curves indicates that there may be high variance in this data. However, since the accuracy still seems to be increasing, we cannot be certain of these conclusions unless we obtain additional data.

One interesting thing to note is that there is a significant dip at 200 training examples, which corresponds to a significant spike in the testing data at the same point. This is likely the result of an odd split of the data. This could be verified by running the data through the algorithm again, but setting a different seed.

\pagebreak
\section{Boosted Decision Trees}\label{performance:boosting}

For the boosted decision trees algorithm, the hyperparameters listed in table \ref{tbl:grid-search:boost} were used in the grid search algorithm.

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table}[htb]
	\centering
	\begin{tabular}{llll}
		\hline
		\multirow{2}{*}{Hyperparameter} 	& \multirow{2}{*}{Values Searched} 	& \multicolumn{2}{l}{Values Chosen} 	\\
		\cline{3-4} 
		&									& QSAR 				& Wine 				\\
		\hline
		Maximum depth 						& 1, 2, ..., 10 					& 9 				& 10 				\\
		Learning rate 						& Values in 0.01 through 1.0 		& 0.64 				& 0.64 				\\
		n Estimators 						& Values in 1 through 100 			& 45 				& 80 				\\
		\hline
	\end{tabular}
	\caption{Grid Search for Boosted Decision Trees}
	
	\label{tbl:grid-search:boost}
\end{table}

\begin{figure*}[tbp]
	\centering
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/Boost_qsar_LC.png}
		\caption{Boosting -- QSAR -- Learning Curve}
		\label{fig:boost-qsar-lc}
	\end{minipage}\hspace{.05\textwidth}
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/Boost_wine_LC.png}
		\caption{Boosting -- Wine Quality -- Learning Curve}
		\label{fig:boost-wine-lc}
	\end{minipage}
\end{figure*}

As was the case with the decision tree model (sans boosting), figures \ref{fig:boost-qsar-lc} and \ref{fig:boost-wine-lc} show that boosting performed similarly on both sets of data. There is a small increase in accuracy for both (on the order of around 5\% increase when all training data is used), but boosting the trees shows an even more extreme overfitting of the training sets.

Another interesting note: The accuracy dip in the wine data that occurred at 200 samples using the non-boosted learner is gone. Instead, there is now an obvious dip in training accuracy at the very next sampling point (around 220 samples). While this did not result in a significant change in the validation curve at the same point (it actually decreased as well), the same hypothesis is drawn: This behavior is likely due to a bad data split, and experimenting with different seed values would prove the hypothesis.

\begin{figure*}[tbp]
	\centering
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/Boost_qsar_ITER_LC.png}
		\caption{Boosting - QSAR - Iterative Learning Curve}
		\label{fig:boost-qsar-lc-iter}
	\end{minipage}\hspace{.05\textwidth}
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/Boost_wine_ITER_LC.png}
		\caption{Boosting - Wine Quality - Iterative Learning Curve}
		\label{fig:boost-wine-lc-iter}
	\end{minipage}
\end{figure*}

Figures \ref{fig:boost-qsar-lc-iter} and \ref{fig:boost-wine-lc-iter} show the effects of varying the number of estimators used by the learner, keeping the other chosen hyperparameters intact. Overfitting occurs at quite low values, without much impact on the validation accuracy. As increasing the estimators is one of the key ways in which boosting can make weak learners perform better, this further supports the observation that boosting only resulted in a modest increase in accuracy for both data sets.

\pagebreak
\section{Artificial Neural Networks}\label{performance:ann}

For neural networks, the grid search algorithm performed an exhaustive search of the parameters listed in table \ref{tbl:grid-search:ann}.

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table*}[htb]
	\centering
	\begin{tabular}{llll}
		\hline
		\multirow{2}{*}{Hyperparameter} 								& \multirow{2}{*}{Values Searched} 	& \multicolumn{2}{l}{Values Chosen} 	\\
																		\cline{3-4} 
		&																& QSAR 								& Wine 				\\
		\hline
		Activation function 	& RELU, logistic 						& RELU 								& RELU 				\\
		Alpha 					& $10^{-9}$ through $10^{1}$ 			& $10^{-3.5}$ 						& $10^{-0.5}$ 		\\
		Hidden layers 			& 1-3 dimensions, 3 sizes per dimension & (41, 41, 41) 						& (11, 11, 11) 		\\
		Initial learning rate 	& Values in 0.000001 through 0.128 		& 0.008 							& 0.128 			\\
		\hline
	\end{tabular}
	\caption{Grid Search for Artificial Neural Networks}
	\label{tbl:grid-search:ann}
\end{table*}

\begin{figure*}[tbp]
	\centering
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/ANN_qsar_LC.png}
		\caption{ANN -- QSAR -- Learning Curve}
		\label{fig:ann-qsar-lc}
	\end{minipage}\hspace{.05\textwidth}
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/ANN_wine_LC.png}
		\caption{ANN -- Wine -- Learning Curve}
		\label{fig:ann-wine-lc}
	\end{minipage}
\end{figure*}
\begin{figure*}
	\centering
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/ANN_qsar_ITER_LC.png}
		\caption{ANN -- QSAR -- Iterative Learning Curve}
		\label{fig:ann-qsar-lc-iter}
	\end{minipage}\hspace{.05\textwidth}
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/ANN_wine_ITER_LC.png}
		\caption{ANN -- Wine Quality -- Iterative Learning Curve}
		\label{fig:ann-wine-lc-iter}
	\end{minipage}
\end{figure*}

Analyzing the learning curves for the ANN algorithm (shown in figures \ref{fig:ann-qsar-lc} and \ref{fig:ann-wine-lc}) shows similar characteristics. The training curves indicate that the algorithms never managed to fit the data very well, indicating high bias and low variance. The narrow gap between the two curves further proves that there is low variance in the data.

The shape of the curves indicate that both sets of data are starting to plateau just prior to using half of the training examples. Using additional examples past that point shows smaller increases in accuracy, and both seem to reach a near peak using around two-thirds of the training examples. It is possible but unlikely that adding more training examples would produce higher levels of accuracy.

Another learning curve to consider is shown in figures \ref{fig:ann-qsar-lc-iter} and \ref{fig:ann-wine-lc-iter}, plotting accuracy versus iterations (which regulate backpropagation). Again, the algorithm shows similar behavior on both data sets: There is a steady increase in accuracy until more than ten iterations are used; after that, the accuracy drops slightly and remains flat from that point onward.

\pagebreak
\section{Support Vector Machines}\label{performance:svm}

To explore the SVM algorithm, the hyperparameters listed in table \ref{tbl:grid-search:svm} were used in the grid search.

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table*}[htb]
	\centering
	\begin{tabular}{lllll}
		\hline
		\multirow{2}{*}{Hyperparameter} 		& \multirow{2}{*}{Kernel} 	& \multirow{2}{*}{Values Searched} 									& \multicolumn{2}{l}{Values Chosen} \\
																																				\cline{4-5}
												& 							&                                                               	& QSAR 			& Wine 				\\
		\hline
		\multirow{2}{*}{C} 						& Linear 					& \multirow{2}{*}{0.001, 0.251, ..., 2.251} 						& 0.251 		& 0.251 			\\
												& RBF 						& 																	& 0.501 		& 0.751 			\\
		\multirow{2}{*}{Maximum iterations} 	& Linear 					& \multirow{2}{*}{-1, $ \frac{125 \times 10^{4}}{\# samples} $} 	& 1185 			& 782 				\\
												& RBF 						& 																	& -1 			& -1 				\\
		\multirow{2}{*}{Tolerance} 				& Linear 					& \multirow{2}{*}{0.01, 0.02, ..., 0.09} 							& 0.08 			& 0.02 				\\
												& RBF 						& 																	& 0.05 			& 0.02 				\\
		Decision function shape 				& RBF 						& One-vs-rest, one-vs-one 											& One-vs-one 	& One-vs-one 		\\
		Gamma 									& RBF 						& Values in $ \frac{1}{\# features} $ to 2.1 						& 0.0244 		& 0.2909 			\\
		\hline
	\end{tabular}
	\caption{Grid Search for Support Vector Machines}
	\label{tbl:grid-search:svm}
\end{table*}

For this analysis, the linear and radial basis function (RBF) kernels were used -- and they have finally produced some interesting results.

\pagebreak[1]
\subsection{Linear Kernel}\label{performance:svm:linear}

\begin{figure*}[tbp]
	\centering
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/SVMLinear_qsar_LC.png}
		\caption{SVM (Linear) -- QSAR -- Learning Curve}
		\label{fig:svmlinear-qsar-lc}
	\end{minipage}\hspace{.05\textwidth}
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/SVMLinear_wine_LC.png}
		\caption{SVM (Linear) -- Wine Quality -- Learning Curve}
		\label{fig:svmlinear-wine-lc}
	\end{minipage}
\end{figure*}

As seen in figure \ref{fig:svmlinear-qsar-lc}, the QSAR data showed a somewhat traditional learning curve. Its accuracy was in line with the other algorithms on the validation set, and there is obvious convergence between the training and validation curves. However, it is interesting to note the high levels of errors within the testing set. This is one of the few algorithms that exhibited high bias and low variance, which means that the models are underfitting the training data. Thus, it is unlikely that adding more data will produce better results.

Even more interesting is the learning curve for the wine data (figure \ref{fig:svmlinear-wine-lc}). Looking at the training curve is fairly baffling, because unlike the other kernels, the training accuracy is remarkably low at the smallest sample sizes. It quickly increases as more samples are introduced, but never really surpasses the validation curve.

It is also likely significant that the accuracy of the validation set is also poor across the board. The gap between the curves is minute, so there is hardly any variance, but it is obvious that there is high bias. The wine data is most assuredly not linearly separable -- at least not with a linear kernel.

%\begin{figure*}
%	\centering
%	\begin{minipage}{0.45\textwidth}
%		\centering
%		\includegraphics[width=\textwidth]{images/SVMLinear_qsar_ITER_LC.png}
%		\caption{SVM (Linear) -- QSAR -- Iterations Learning Curve}
%		\label{fig:svmlinear-qsar-lc-iter}
%	\end{minipage}\hspace{.05\textwidth}
%	\begin{minipage}{0.45\textwidth}
%		\centering
%		\includegraphics[width=\linewidth]{images/SVMLinear_wine_ITER_LC.png}
%		\caption{SVM (Linear) -- Wine Quality -- Iterations Learning Curve}
%		\label{fig:svmlinear-wine-lc-iter}
%	\end{minipage}
%\end{figure*}


%\begin{figure}[h]
%	\centering
%	\includegraphics[width=0.9\linewidth,keepaspectratio]{images/SVMLinear_qsar_ITER_LC.png}
%	\caption{Iterations Learning Curve for SVM Algorithm (Linear Kernel) - QSAR}
%	\label{fig:svmlinear-qsar-lc-iter}
%\end{figure}
%
%\begin{figure}[h]
%	\centering
%	\includegraphics[width=0.9\linewidth,keepaspectratio]{images/SVMLinear_wine_ITER_LC.png}
%	\caption{SVM (Linear) -- Wine Quality -- Iterative Learning Curve}
%	\label{fig:svmlinear-wine-lc-iter}
%\end{figure}

\pagebreak[1]
\subsection{RBF Kernel}\label{performance:svm:rbf}

\begin{figure*}[tbp]
	\centering
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/SVM_RBF_qsar_LC.png}
		\caption{SVM (RBF) -- QSAR -- Learning Curve}
		\label{fig:svmrbf-qsar-lc}
	\end{minipage}\hspace{.05\textwidth}
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/SVM_RBF_wine_LC.png}
		\caption{SVM (RBF) -- Wine Quality -- Iterative Learning Curve}
		\label{fig:svmrbf-wine-lc}
	\end{minipage}
\end{figure*}

The curves for the QSAR data shown in figure \ref{fig:svmrbf-qsar-lc} show convergence, with validation accuracy increasing rapidly through approximately 200 samples. There is a moderate amount of distance between the curves still, signifying a higher variance using RBF. This leads to the conclusion that the linear kernel is better suited to the QSAR data set.

Figure \ref{fig:svmrbf-wine-lc} shows the curve for the wine data, and this time we see a more traditional result than we did with the linear kernel. The model was able to fit well at small samples, but quickly underfits as more samples are added in. However, there is obvious convergence occurring, so we are unlikely to see better results with more data. The RBF kernel is clearly the better of the two kernels for this task.

%\begin{figure}[h]
%	\centering
%	\includegraphics[width=0.9\linewidth,keepaspectratio]{images/SVM_RBF_qsar_ITER_LC.png}
%	\caption{SVM (RBF) -- QSAR -- Iterative Learning Curve}
%	\label{fig:svmrbf-qsar-lc-iter}
%\end{figure}

%\begin{figure}[h]
%	\centering
%	\includegraphics[width=0.9\linewidth,keepaspectratio]{images/SVM_RBF_wine_ITER_LC.png}
%	\caption{SVM (RBF) -- Wine Quality -- Iterative Learning Curve}
%	\label{fig:svmrbf-wine-lc-iter}
%\end{figure}

\pagebreak
\section{$k$-Nearest Neighbors}\label{performance:knn}

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table*}[htb]
	\centering
	\begin{tabular}{llll}
		\hline
		\multirow{2}{*}{Hyperparameter} 	& \multirow{2}{*}{Values Searched} 	& \multicolumn{2}{l}{Values Chosen} 	\\
																				\cline{3-4} 
											&									& QSAR 				& Wine 				\\
		\hline
		Metric 								& Manhattan, Euclidean, Chebyshev 	& Euclidean 		& Euclidean 		\\
		\# neighbors 						& 1, 4, ..., 51 					& 22 				& 1 				\\
		\hline
	\end{tabular}
	\caption{Grid Search for $k$-Nearest Neighbors}
	\label{tbl:grid-search:knn}
\end{table*}

Table \ref{tbl:grid-search:knn} shows the hyperparameters searched and chosen for the $k$NN algorithm.

It is interesting to note that the wine data performed poorly at any values of $k$ above 1. Experimenting with other values showed that this was not an error; performance with $k$-values above one were less successful. This already indicates that the algorithm is likely not going to find any hidden relationships within the data.

\begin{figure*}[tbp]
	\centering
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/KNN_qsar_LC.png}
		\caption{$k$-Nearest Neighbors -- QSAR -- Learning Curve}
		\label{fig:knn-qsar-lc}
	\end{minipage}\hspace{.05\textwidth}
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth,keepaspectratio]{images/KNN_wine_LC.png}
		\caption{$k$-Nearest Neighbors -- Wine Quality -- Learning Curve}
		\label{fig:knn-wine-lc}
	\end{minipage}
\end{figure*}

The learning curve for the QSAR data (figure \ref{fig:knn-qsar-lc}) shows that this learner had a very difficult time fitting to the data. There is a high amount of bias and low variance.

We see the opposite behavior on the wine data. There is 100\% accuracy on the training set across the range of sample sizes, and the validation curve shows a continuous increase in accuracy as more samples are added, with no sign of convergence. More data would definitely help this learner to find a better solution.

\pagebreak
\section{Comparison of algorithms}\label{comparison-of-algorithms}

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
% \usepackage{graphicx}
\begin{table}[htb]
	\centering
	%\resizebox{\textwidth}{!}{%
	\begin{tabular}{lrr}
		\hline
		\multicolumn{1}{c}{\multirow{2}{*}{Algorithm}} 	& \multicolumn{2}{c}{Accuracy} 							\\
														\cline{2-3} 
		\multicolumn{1}{c}{} 							& \multicolumn{1}{c}{QSAR} 	& \multicolumn{1}{c}{Wine} 	\\
		\hline
		Decision trees 									& 83.37\% 					& 72.61\% 					\\
		Boosted decision trees 							& 80.58\% 					& 75.98\% 					\\
		Artificial neural networks 						& 86.24\% 					& 74.34\% 					\\
		Support vector machine (RBF kernel) 			& 84.75\% 					& 75.90\% 					\\
		Support vector machine (linear kernel) 			& 86.87\% 					& 73.26\% 					\\
		k-nearest neighbors 							& 84.39\% 					& 74.05\% 					\\
		\hline
	\end{tabular}%
	%}
	\caption{Summary of Accuracy Results}
	\label{tbl:test-results:accuracy}
\end{table}

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
% \usepackage{graphicx}
\begin{table*}[htb]
	\centering
	\begin{tabular}{lrrrrrr}
		\hline
		\multicolumn{1}{c}{\multirow{2}{*}{Algorithm}} 	& \multicolumn{3}{c}{QSAR} 			& \multicolumn{3}{c}{Wine} 			\\
		\cline{2-7} 
		\multicolumn{1}{c}{} 							& Training 	& Testing 	& Sum 		& Training 	& Testing 	& Sum 		\\
		\hline
		Decision trees 									& 0.00993 	& 0.00014 	& 0.01008 	& 0.00772 	& 0.00013 	& 0.00785  	\\
		Boosted decision trees 							& 0.50452 	& 0.00220 	& 0.50672 	& 0.63053 	& 0.00400 	& 0.63453  	\\
		Artificial neural networks 						& 2.54344 	& 0.00703 	& 2.55046 	& 0.03494 	& 0.00018 	& 0.03512 	\\
		Support vector machine (RBF kernel) 			& 0.01791 	& 0.00170 	& 0.01962 	& 0.03002 	& 0.00255 	& 0.03257 	\\
		Support vector machine (linear kernel) 			& 0.04535 	& 0.00201 	& 0.04736 	& 0.01857 	& 0.00165 	& 0.02018	\\
		$k$-nearest neighbors 							& 0.00139 	& 0.02407 	& 0.02545 	& 0.00115 	& 0.01025 	& 0.01140 	\\
		\hline
	\end{tabular}
	\caption{Summary of Timing Results}
	\label{tbl:test-results:timing}
\end{table*}

Figure \ref{tbl:test-results:accuracy} summarizes the accuracy results when using each algorithm and the given hyperparameters against the final set of testing data -- data that the algorithm had never seen. Surprisingly, there is very little difference in the results.

The wine data set showed particularly less differentiation in accuracy. The worst accuracy came from using decision trees -- but when those trees were made into an ensemble learner, they performed the best against the data.

That performance came at a cost of time, however. As shown in table \ref{tbl:test-results:timing}, the sum time to use boosted trees with the wine data was 634.5~ms per sample, as opposed to 7.8~ms for plain trees. It takes boosted trees 80 times longer to produce an accuracy increase of only 3\%.

Time is also a differentiating factor for the QSAR data -- notably with the boosted tree and neural network algorithms. The training time for the boosting algorithm was comparable to that of the wine data, which suggests that the number of samples in the data affects the time performance of that algorithm more than the number of features present. This could be easily proven with additional sets of data, but is beyond the scope of this project.

The most notable data point in regards to timing is how poorly the ANN algorithm performed on the QSAR data. At 2.5~seconds per sample, its performance was four times worse than the next-worse performance case -- and it only had the second-highest accuracy on that data set. This timing is likely due to the number of features present in the data. The optimal network was determined to be three layers of 41 neurons each (for $ 41^{3} = 68921 $ total). In comparison, the network for the wine data contained only $ 11^{3} = 1331 $ neurons.

\[ \frac{41^{3}}{11^{3}} = \frac{68921}{1331} = 51.78 \]

The number of neurons used for QSAR data was nearly 52 times that used for the wine data. In comparison, the time needed to process both sets of data is $ \frac{2.55046}{0.03512} = 72.62 $. It therefore seems likely that this performance disparity is explained by the network size used. The performance could be improved through feature engineering, assuming that it is possible to somehow combine multiple features in a manner that does not decrease the information available to the model.

\smallskip

In conclusion, the learners had better accuracy with the QSAR data overall. This surprised me, as I had hoped that its additional features and less-balanced final class would make it the harder problem to solve. My intuition about what is ``hard'' was proven wrong, and I conclude that these two sets of data are likely not very interesting at all.


% use section* for acknowledgment
\ifCLASSOPTIONcompsoc
% The Computer Society usually uses the plural form
\section*{Acknowledgments}
\else
% regular IEEE prefers the singular form
\section*{Acknowledgments}
\fi

Thanks to Chad Maron for his code\cite{cmaron-code}, which did most of the heavy lifting.

Likewise, thanks to Jonathan Tay\cite{jontay-code}, on which Chad's code is based.

\bibliography{mhobson7-analysis}{}
\bibliographystyle{plain}

\end{document}
