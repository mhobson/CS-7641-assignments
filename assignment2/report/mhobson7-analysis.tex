\documentclass[12pt,journal]{IEEEtran}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{comment}
% Convert qutoes to smart ones (in case any get left in accidentally).
\usepackage [english]{babel}
\usepackage [autostyle, english = american]{csquotes}
\MakeOuterQuote{"}

\usepackage{cite}

% subcaption for subfigure environment
\usepackage{subcaption}

\title{Project 2: Randomized Optimization}

\author{Matt~Hobson \\ {\small mhobson7@gatech.edu}}

\begin{document}

\maketitle


\section{Find Optimal Weights for an Artificial Neural Network}\label{nn}

The first part of this project is to compare the backpropagation algorithm used to find optimal weights for a neural network with several random search algorithms.

\subsection{Data}\label{data}

The data used was the QSAR bio-degradation data set\cite{qsar-data}, whose task is to attempt to find relationships between a chemical's structure and its biodegradability to develop QSAR models. Table \ref{tbl:data-set-stats} contains a summary of its primary characteristics.

\begin{table}[htb]
	\centering
	\begin{tabular}{lc}
		\hline
		Data set 					& QSAR 			\\
		\hline	
		Records 					& 1055 			\\
		Features 					& 41 			\\
		Class distribution (\%) 	& 66.3, 33.7 	\\
		Sparse 						& No 			\\
		Binary 						& Yes 			\\
		Balanced\cite{239982} 		& Yes 			\\
		\hline
	\end{tabular}
	\caption{Data Set Statistics}
	\label{tbl:data-set-stats}
\end{table}

The data is complete, and all of its features are numeric and continuous. The data was shuffled five times to remove any inherent ordering, and its final binary class was converted to ``1'' for ``RB'' and ``-1'' otherwise.

\subsection{Methodology}\label{methodology}

The ABAGAIL library\cite{abagail-code} was used in conjunction with Jython 2.7.1 to perform these experiments. To compare the algorithms, an artificial neural network was created, with parameters found to be optimal in Project 1. The neural network used was comprised of three layers with 41 nodes in each.

Because ABAGAIL does not implement cross-validation, the data set was split into three sets. First, 20\% of the data was set aside for final testing. Then the remaining data was split again, with 80\% (or 64\% of the original data) set aside for model training and the remainder for model validation. The data splittings were preserved across runs, ensuring that the same data was used in all experiments.

Each algorithm was used inside the network for selecting the best weights of the nodes within each layer. For backpropagation, the  ``RELU'' activation function was used with an initial learning rate of $0.008$ (as was found optimal in Project 1).

\subsection{Results}\label{nn:results}

Figure \ref{fig:nn:best_fitness} shows a comparison of the F1 scores achieved using all four algorithms.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.9\linewidth,keepaspectratio]{images/NN/Best_Fitness.pdf}
	\caption{F1 scores for each algorithm at increasing iteration values.}
	\label{fig:nn:best_fitness}
\end{figure}

The results show some interesting characteristics. First, given enough iterations, simulated annealing noticeably outperforms all other algorithms (including backpropagation) on this measure of ``best''. It takes quite a while to get there, but eventually finds the most accurate weights for the network.

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
% \usepackage{graphicx}
\begin{table*}[htb]
	\centering
	\begin{tabular}{llrrr}
		\hline
		Algorithm 					& Parameters 	& Best fitness 	& Iterations 	& Time 		\\
		\hline
		Backpropagation 			& 				& 0.755556 		&   70 			&  0.8077 	\\
		Genetic 					& 50, 20, 10 	& 0.787879 		&  600 			& 88.1715 	\\
		Randomized hill climbing 	& 				& 0.744526 		& 1340 			&  4.4048 	\\
		Simulated annealing 		& 0.15 			& 0.796875 		& 2180 			&  7.2064 	\\
		\hline
	\end{tabular}
	\caption{ Additional measures of each algorithm's best score on the neural-network weights problem. }
	\label{tbl:nn:results}
\end{table*}

\begin{figure*}[tbp]
	\centering
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/NN/Backprop_Fitness.pdf}
		\caption{Backpropagation}
		\label{fig:nn:crossvalidation:backprop}
	\end{subfigure}
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/NN/GA_50_Score.pdf}
		\caption{Genetic algorithm}
		\label{fig:nn:crossvalidation:ga}
	\end{subfigure}
	\smallskip
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/NN/RHC_Fitness.pdf}
		\caption{Random hill climbing algorithm}
		\label{fig:nn:crossvalidation:rhc}
	\end{subfigure}
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/NN/SA_Accuracy.pdf}
		\caption{Simulated annealing}
		\label{fig:nn:crossvalidation:sa}
	\end{subfigure}
	\caption{Training and validation scores at increasing iteration values.}
	\label{fig:nn:crossvalidation}
\end{figure*}

However, you could also consider the genetic algorithm as a ``best''. It reached a comparable F1 score using far fewer iterations, and seemed to converge towards its most accurate scores earlier. This is clearly shown in figure \ref{fig:nn:crossvalidation}, which shows training and validation curves. The genetic algorithm begins to converge somewhere around 250 iterations (as seen in figure \ref{fig:nn:crossvalidation:ga}), whereas simulated annealing converges closer to 400 iterations (figure \ref{fig:nn:crossvalidation:sa}) -- and begins to overfit beyond that point. Also at that point of convergence, SA's accuracy is far lower than the genetic algorithm.

It is important to note that any comparisons to backpropagation are highly suspect and not discussed here. Using the parameters from project 1 as directed for this project causes the network to overfit almost immediately (as seen in figure \ref{fig:nn:crossvalidation:backprop}), and further exploration of why this occurred (especially with respect to how the grid search was performed) would need to be done to make the comparison meaningful.

A final consideration is time. As shown in table \ref{tbl:nn:results}, the genetic algorithm takes significantly longer per iteration to compute in comparison with the other algorithms considered. This is of course in keeping with the algorithm's design, but is something to consider when deciding on a ``best'' performer.

Considering all factors, the genetic algorithm seems to be the best overall performer. It achieves the highest accuracy in the fewest iterations. Even when you factor in the time per iteration, it does not seem to cause any overfitting even after it achieves its high accuracy level, which is fairly ideal for a learning algorithm.



\section{Additional Comparison of the Random Search Algorithms}\label{toy-problems}

As directed in the project description, three optimization problems were created in order to further compare these random search algorithms. The three algorithms previously described were tested against each problem, with the MIMIC search algorithm\cite{DeBonet:1996:MFO:2998981.2999041} being considered as well.


\subsection{ Methodology }\label{toy-problems:methodology}

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table*}[htb]
	\centering
	\begin{tabular}{lll}
		\hline
		Search algorithm 			& Parameter 		& Values explored 				\\
		\hline
		\multirow{2}{*}{Genetic} 	& Mating rate 		& 10, 30, 50 (of 100) 			\\
									& Mutation rate 	& 10, 30, 50 (of 100) 			\\
		MIMIC 						& $m$ (smoothing) 	& 0.1, 0.3, 0.5, 0.7, 0.9 		\\
		Random hill climbing 		& \multicolumn{2}{l}{\textsl{None}} 				\\
		Simulated annealing 		& Cooling exponent 	& 0.15, 0.35, 0.55, 0.75, 0.95 	\\
		\hline
	\end{tabular}
	\caption{ The hyper-parameters that were explored in each problem space for each algorithm. }
	\label{tbl:toy-problems:params-explored}
\end{table*}

For all of the optimization problems below, the algorithms were tested with a variety of parameters as listed in table \ref{tbl:toy-problems:params-explored}. Once those were explored, the parameters which produced the highest overall fitness value were chosen as the best parameters to use for that algorithm with that problem.


\subsection{Traveling Salesperson}\label{toy-problems:tsp}

The ``traveling salesperson'' problem is a classic problem of finding the shortest distance connecting a given set of points. This, of course, presents a problem since the algorithms which we are testing all want to maximize a function, but this is solved by finding the maximum of the inverse of the distance.

This problem should highlight the genetic algorithm for a couple of reasons. First, GAs perform very well when locality and groupings matter, as they tend to ``hang on'' to optimal sub-spaces within the problem space. As an example in this problem space, once it finds an ideal order between three points, the crossover mechanism of the algorithm is likely to keep those three points together for quite some time, and will also begin combining more and more of these sub-spaces together.

The mutation aspect of genetic algorithms will also help. It might seem a hindrance at first, especially in light of the previous point. However, the fitness function for this problem will react brutally (in the form of an extremely low fitness score) when a mutation causes a good sub-space to go bad.

\begin{figure*}[htbp]
	\centering
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/TSP/Best_Fitness.pdf}
		\caption{Fitness}
		\label{fig:tsp:fitness}
	\end{subfigure}
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/TSP/Best_Fevals.pdf}
		\caption{Function evaluations}
		\label{fig:tsp:fevals}
	\end{subfigure}
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/TSP/Best_time.pdf}
		\caption{Computational time}
		\label{fig:tsp:time}
	\end{subfigure}
	\caption{ Results for the ``traveling salesperson'' problem. }
	\label{fig:tsp}
\end{figure*}

The fitness for the algorithms are shown in figure \ref{fig:tsp:fitness}, and show that GA far outperformed the other algorithms in this regard. It scores significantly higher with very few iterations and maintains that scoring gap throughout the experiment.

The genetic algorithm needs significantly more function evaluations to obtain this accuracy (as shown in figure \ref{fig:tsp:fevals}), but that is in line with the algorithm's design, since at each iteration it is evaluating a significant number of new individuals within its population.

Interestingly, the MIMIC algorithm never seems to grasp this problem space, and performs the worst in every category analyzed here (fitness, time, and the number of evaluations).


\subsection{Continuous Peaks}\label{toy-problems:contpeaks}

The ``continuous peaks'' problem is essentially to find the longest consecutive strings of the same bit repeated at the beginning or end of the string (a continuous ``peak''). The fitness function includes a parameter $T$ which dictates a length of continuous bits that must be present at both the beginning and end of the bit-string, to get an additional reward.

For our problem, we use a string of 100 randomly-set bits, and the reward threshold $T$ is set to 29. This problem space has four global maxima: 70 consecutive bits at one end and 30 at the other end. The maximum fitness, therefore, is 170 (70 for the longest string at front or back greater than $T$, plus 100 because both ends are above $T$).

This problem should highlight the simulated annealing algorithm. This problem can have multiple global optima present, as well as additional local optima. This presents a problem for most random-search algorithms, which can easily get stuck in one of the local optima spaces.

In contrast, simulated annealing thrives here, due to its ability (and desire) to explore the space early -- even when it has an idea of where an optimum might be, there is always that chance (depending on the algorithm's current temperature) that it will go the ``wrong'' way anyway just to see if it can find another, higher ``hill'' to climb.

\begin{figure*}[htbp]
	\centering
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/CONTPEAKS/Best_Fitness.pdf}
		\caption{Fitness}
		\label{fig:contpeaks:fitness}
	\end{subfigure}
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/CONTPEAKS/Best_Fevals.pdf}
		\caption{Function evaluations}
		\label{fig:contpeaks:fevals}
	\end{subfigure}
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/CONTPEAKS/Best_time.pdf}
		\caption{Computational time}
		\label{fig:contpeaks:time}
	\end{subfigure}
	\caption{ Results for the ``continuous peaks'' problem. }
	\label{fig:contpeaks}
\end{figure*}

As shown in figure \ref{fig:contpeaks:fitness}, simulated annealing was able to achieve the highest fitness score as predicted. Interestingly, the genetic algorithm shows comparable fitness at the end, and even better fitness at earlier stages. This is likely due to similar reasons as those mentioned in section \ref{toy-problems:tsp} for the ``traveling salesperson'' problem: GAs like to maintain good groupings and mate those with other good groupings. Since the ultimate goal of this problem is to find the largest group of similar bits at the ends, it makes sense that the genetic algorithm performs well here.

However, GAs also take more time (figure \ref{fig:contpeaks:time}) and perform more evaluations (figure \ref{fig:contpeaks:fevals}) that simulated annealing needs to perform well on this problem -- and both increases are on the order of two magnitudes more than SA. This makes it easy to conclude that simulated annealing is the best algorithm of the four tested on this problem.


\subsection{Flip-Flop}\label{toy-problems:flipflop}

The final problem is the ``flip-flop'' problem, which is nearly the opposite of ``continuous peaks''. The problem space is again a string of bits (this time we use 1000 bits with randomly chosen initial values), but for this problem, the fitness function is to maximize the number of times when the value of the next bit in the string is flipped from the value of the current bit. Thus, for a bit-string of length $N$, the maximum fitness is $N-1$ and occurs when every bit is the opposite of both neighbors.

The ``flip-flop'' problem should highlight the MIMIC algorithm, because solving this problem is more about finding the structure of the relationships between the individual bits than anything else. If locality was a factor then a genetic algorithm would likely do well, but this problem cares more about how bit $n$ relates to its ``parent'', regardless of where either bit is located within the string.

In fact, the optimal dependency tree ends up mapping a single relationship: bit $n$ should be the opposite of its parent. That's the ultimate goal.

\begin{figure*}[htbp]
	\centering
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/FLIPFLOP/Best_Fitness.pdf}
		\caption{Fitness}
		\label{fig:flipflop:fitness}
	\end{subfigure}
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/FLIPFLOP/Best_Fevals.pdf}
		\caption{Function evaluations}
		\label{fig:flipflop:fevals}
	\end{subfigure}
	\begin{subfigure}{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/FLIPFLOP/Best_time.pdf}
		\caption{Computational time}
		\label{fig:flipflop:time}
	\end{subfigure}
	\caption{ Results for the ``flip-flop'' problem. }
	\label{fig:flipflop}
\end{figure*}

As shown in figure \ref{fig:flipflop:fitness}, MIMIC is indeed able to achieve the highest fitness score. And it does so quickly, although MIMIC of course does a tremendous amount of computation per iteration.

An interesting note is how poorly the genetic algorithm does -- it barely does better than chance, quickly getting to and then staying at just under 600 for its fitness scores. It seems a bit counter-intuitive, given that this problem is all about relationships between nearby neighbors. However, it seems that the crossovers and mutations become a hindrance after a certain point. This makes some sense, because GAs keep trying to expand the related groupings while keeping the groupings' positions within the string as intact as possible. But for this problem, where each group has no bearing on fitness.

As mentioned, MIMIC does a good jump of recognizing and reducing the dependencies between the bits, which is why it greatly outperforms all of the other algorithms in this problem space.


% use section* for acknowledgment
\ifCLASSOPTIONcompsoc
% The Computer Society usually uses the plural form
\section*{Acknowledgments}
\else
% regular IEEE prefers the singular form
\section*{Acknowledgments}
\fi

The base of the code used for this project is from Chad Maron\cite{cmaron-code} and Jonathan Tay\cite{jontay-code}; many thanks to them both. Thanks also to the authors and maintainers of the ABAGAIL library\cite{abagail-code}.

\bibliography{mhobson7-analysis}{}
\bibliographystyle{plain}

\end{document}
