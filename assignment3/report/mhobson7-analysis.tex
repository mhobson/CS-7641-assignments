\documentclass[11pt,journal]{IEEEtran}

\usepackage{graphicx}

\usepackage{multirow}

\usepackage{comment}

% Convert qutoes to smart ones (in case any get left in accidentally).
\usepackage [english]{babel}
\usepackage [autostyle, english = american]{csquotes}
\MakeOuterQuote{"}

\usepackage{cite}

\usepackage[font=small]{caption}

% subcaption for subfigure environment
\usepackage{subcaption}

% package to allow positioning figured Here
\usepackage{float}
% options to set float separation values from defaults
% - \floatsep: space left between floats (12.0pt plus 2.0pt minus 2.0pt).
% - \textfloatsep: space between last top float or first bottom float and the text (20.0pt plus 2.0pt minus 4.0pt).
% - \intextsep : space left on top and bottom of an in-text float (12.0pt plus 2.0pt minus 2.0pt).
% - \dbltextfloatsep is \textfloatsep for 2 column output (20.0pt plus 2.0pt minus 4.0pt).
% - \dblfloatsep is \floatsep for 2 column output (12.0pt plus 2.0pt minus 2.0pt).
% - \abovecaptionskip: space above caption (10.0pt).
% - \belowcaptionskip: space below caption (0.0pt).
%\setlength{\floatsep}{4pt}
%\setlength{\textfloatsep}{4pt}
%\setlength{\intextsep}{4pt}
%\setlength{\abovecaptionskip}{4pt}
%\setlength{\belowcaptionskip}{4pt}

% algorithms: https://en.wikibooks.org/wiki/LaTeX/Algorithms
% The algorithmicx package provides a number of popular constructs for algorithm designs. Put \usepackage{algpseudocode} in the preamble to use the algorithmic environment to write algorithm pseudocode (\begin{algorithmic}...\end{algorithmic}). You might want to use the algorithm environment (\usepackage{algorithm}) to wrap your algorithmic code in an algorithm environment (\begin{algorithm}...\end{algorithm}) to produce a floating environment with numbered algorithms.
\usepackage{algorithm}
\usepackage{algpseudocode}

% arydshln: dashed lines in array/tabular: https://ctan.org/pkg/arydshln
\usepackage{arydshln}

\usepackage{xcolor}

% quoting: allows to indent (and italicize in this case) paragraphs
%\begin{quoting}
%\end{quoting}
\usepackage[font=itshape]{quoting}


\title{Assignment 3: Unsupervised Learning and Dimensionality Reduction}

\author{
	Matt~Hobson \\
	{\small mhobson7@gatech.edu}
}

\begin{document}

\maketitle


\section*{Introduction}\label{intro}

The purpose of this assignment is to explore two techniques for unsupervised learning: clustering and dimensionality reduction. Several popular algorithms of each technique will be analyzed and compared, including a practical comparison utilizing a traditional neural-network learner.

\subsection{Data Sets}\label{intro:data}

The data sets used for this assignment are the same from assignment 1. ``QSAR Biodegradation''\cite{qsar-data} aims to develop QSAR models by finding relationships between the structure and biodegradability of chemicals. ``Wine Quality''\cite{wine-data} hopes to find objective measures of wine quality ratings. As in previous assignments, these ratings were converted to binary values based on median rating value.

A summary of the primary characteristics of the data sets is presented in table \ref{tbl:data-set-stats}.

\begin{table}[htbp]
	\centering
	\begin{tabular}{lcc}
		\hline
		Data set 					& QSAR 			& Wine 			\\
		\hline
		Records 					& 1055 			& 1599 			\\
		Features 					& 41 			& 11 			\\
		Class distribution (\%) 	& 66.3, 33.7 	& 46.5, 53.5 	\\
		Sparse 						& No 			& No 			\\
		Binary 						& Yes 			& Yes 			\\
		\hline
	\end{tabular}
	\caption{Data set statistics.}
	\label{tbl:data-set-stats}
\end{table}


\section{Clustering}\label{clustering}

The first portion of the assignment explores feature clustering using $k$-means and expectation-maximization algorithms.

\subsection{Methodology}\label{clustering:methodology}

A Gaussian mixture model (GMM) was chosen as the specific implementation of the expectation-maximization algorithm for this project. GMM is a very popular EM technique because it is fast and generally provides good clustering results across a spectrum of data problems.

\begin{figure*}[tbp]
	\centering
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/benchmark/qsar_GMM_combined.pdf}
		\caption{GMM clustering for QSAR data.}
		\label{fig:benchmark:qsar:GMM_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/benchmark/wine_GMM_combined.pdf}
		\caption{GMM clustering for wine data.}
		\label{fig:benchmark:wine:GMM_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/benchmark/qsar_Kmeans_combined.pdf}
		\caption{$k$-means clustering for QSAR data.}
		\label{fig:benchmark:qsar:kmeans_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/benchmark/wine_Kmeans_combined.pdf}
		\caption{$k$-means clustering for wine data.}
		\label{fig:benchmark:wine:kmeans_combined}
	\end{subfigure}
	\caption{Combined clustering benchmarks.}
	\label{fig:benchmark:combined}
\end{figure*}

Both algorithms were run with cluster values ranging from 2 through 40, increasing by one through 10 clusters and by 5 thereafter. Several calculations were made for each clustering, including adjusted mutual information (AMI), silhouette score, and an accuracy metric calculated by using the \textsl{accuracy\_score} metric from scikitlearn\cite{scikit-learn}. Additionally, the Bayesian Information Criterion (BIC) was calculated for GMM, and the sum of squared error (SSE) was calculated for $k$-means. These values are plotted in figure \ref{fig:benchmark:combined}.

\subsection{Analysis}\label{clustering:analysis}

\subsubsection*{GMM}
Looking at figure \ref{fig:benchmark:qsar:GMM_combined}, there is a good amount of chaos for cluster values of 8 and lower, which suggests that none of those values would be good candidates. The clustering accuracy line shows that choosing 25 clusters produces the best representation of the original features; also accuracy remains consistently high (above 0.8) from that value onward, so it is unlikely that we would want to choose any values below 25. We also see a dip in BIC at 25, indicating that it may be the best choice to use.

Switching to the wine data (figure \ref{fig:benchmark:wine:GMM_combined}), the GMM algorithm reached its highest accuracy with 30-35 clusters -- triple the number of original features -- but accuracy tended to hover around 0.7 for 15 and more clusters, suggesting that anything in that range is likely as good as any other.

\subsubsection*{$k$-means}
Looking first at the QSAR data (figure \ref{fig:benchmark:qsar:kmeans_combined}), accuracy rose above 0.8 at 20 clusters and remained near that accuracy value as more clusters were considered, indicating that using less than 20 clusters is likely the wrong choice. Interestingly, the AMI and silhouette values changed inversely at and above 10 clusters; if one increased, the other decreased. This is intriguing, but I cannot find a good explanation for this behavior.

Figure \ref{fig:benchmark:wine:kmeans_combined} shows that the algorithm produced similar accuracy results but at smaller cluster sizes. In fact, most of the measures reach a fairly steady state at around 8 clusters. Further, the SSE line shows an elbow at nearly that same value, and while SSE continues to decrease, this would be a good value to consider.

\subsubsection*{Conclusion} 
The GMM algorithm seems to have issues with small numbers of clusters. This was surprising; since $k$-means performs ``hard'' cluster assignments, my expectation was that it would be likely to have more highly variable performance with fewer clusters and that the ``soft'' method of GMM would perform better. What could account for that is the fact that $k$-means uses rigid, spherical clusters, whereas GMM is not bound to any shape in the default configuration (as used in these experiments); thus $k$-means could potentially adapt its clusters better with smaller numbers.

\section{Dimensionality Reduction}\label{dim-reduction}

Next is the exploration of dimensionality-reduction techniques using four different algorithms. The three mandatory algorithms are primary component analysis (PCA), independent component analysis (ICA), and randomized projections (RP). For the fourth algorithm, I chose to use a random forest (RF) classifier.

\subsection{Methodology}\label{dim-reduction:methodology}

\begin{table*}[tbp]
	\centering
	\begin{tabular}{lll}
		\hline
		Algorithm & Measure                 & Dimension values considered                              \\
		\hline
		PCA       & Variance of eigenvalues & 1 through the maximum number of original features.       \\
		ICA       & Mean kurtosis           & 1 through 60.                                            \\
		RP        & Reconstruction error    & 1 through 60.                                            \\
		RF        & Feature importance      & 1 through one less than the number of original features. \\
		\hline
	\end{tabular}
	\caption{Ranges of dimension considered for each algorithm, and the properties measured for each.}
	\label{tbl:dim-reduction:ranges-properties}
\end{table*}

To try and determine the best number of components to which each set of samples should be reduced by each algorithm, scree plots were generated with different measures depending on the algorithm. Those measures are listed in table \ref{tbl:dim-reduction:ranges-properties} (as well as how many dimensions were tested for each algorithm).

For all algorithms, the ``knee'' values were calculated with the kneedle\cite{kneedle} method as implemented in the kneed\cite{kneed} library.

The results for all algorithms and data sets are shown in figure \ref{fig:dim-reduction}.

\begin{figure*}[tbp]
	\centering
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/PCA/qsar_scree.pdf}
		\caption{PCA results for QSAR data.}
		\label{fig:dim-reduction:pca:qsar}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/PCA/wine_scree.pdf}
		\caption{PCA results for wine data.}
		\label{fig:dim-reduction:pca:wine}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/ICA/qsar_scree.pdf}
		\caption{ICA results for QSAR data.}
		\label{fig:dim-reduction:ica:qsar}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/ICA/wine_scree.pdf}
		\caption{ICA results for wine data.}
		\label{fig:dim-reduction:ica:wine}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RP/qsar_scree_2.pdf}
		\caption{RP results for QSAR data.}
		\label{fig:dim-reduction:rp:qsar}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RP/wine_scree_2.pdf}
		\caption{RP results for wine data.}
		\label{fig:dim-reduction:rp:wine}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RF/qsar_scree.pdf}
		\caption{RF results for QSAR data.}
		\label{fig:dim-reduction:rf:qsar}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RF/wine_scree.pdf}
		\caption{RF results for wine data.}
		\label{fig:dim-reduction:rf:wine}
	\end{subfigure}
	\caption{Scree charts for dimensionality-reduction algorithms.}
	\label{fig:dim-reduction}
\end{figure*}

\subsection{Analysis}\label{dim-reduction:analysis}

\subsubsection*{PCA}
Looking at figures \ref{fig:dim-reduction:pca:qsar} and \ref{fig:dim-reduction:pca:wine}, there is not much of interest to see in either chart. The wine data is nearly perfect, and although visually the elbow could be considered to occur at 2, choosing 3 is just as suitable. For QSAR, there are three of four potential elbows, but again 6 seems the best of those values since it occurs farthest from the line connecting the min and max values.

However, the chosen values seem quite low. Given that both data sets are completely comprised of numeric features, going from 41 features to 6 dimensions (and 11 to 3) would seem to lose a significant amount of useful information. Then again, perhaps the features themselves have less useful information in the first place -- a theory supported by the fact that previous experiments have found that this data is not easy to learn.

\subsubsection*{ICA}
The ICA charts (figures \ref{fig:dim-reduction:ica:qsar} and \ref{fig:dim-reduction:ica:wine}) are much more interesting, as there are several well-defined local maxima for both data sets. This provides several candidates for a good knee value; for example, an argument could be made for choosing 7 or 10 components. However, if two points seem to be logical choices, the lesser number should always win, so using the library's selected value seems to be the best choice for both sets of data.

\subsubsection*{RP}
RP was performed ten times to lessen the chance that we repeatedly used bad projections, but the resulting plots of reconstruction error made selecting a good value quite difficult. The goal is to lower error rates while keeping the dimensions low, and figures \ref{fig:dim-reduction:rp:qsar} and \ref{fig:dim-reduction:rp:wine} show that there is a nearly linear trade-off between the two for both sets of data. The kneedle algorithm does as good of a job of selecting a value as anything in this case. An argument could be made to choose 10 for the wine data, but the ``curve'' is so nearly linear that I'd rather trust the algorithm.

\subsubsection*{RF}
The QSAR results for the random forest classifier (figure \ref{fig:dim-reduction:rf:qsar}) show a near-perfect example of an elbow, so there is no doubt of the proper value to pick. Figure \ref{fig:dim-reduction:rf:wine} is not as clear, but the two candidate values seem equidistant from the min-max line, so choosing fewer dimensions is the right choice.

\subsection{Validation}\label{dim-reduction:validation}

\begin{figure*}[tbp]
	\centering
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/benchmark/qsar_tsne.pdf}
		\caption{Original 41 features.}
		\label{fig:tsne:qsar:benchmark}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/PCA/qsar_tsne.pdf}
		\caption{PCA reduction to 6 components.}
		\label{fig:tsne:qsar:pca}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/ICA/qsar_tsne.pdf}
		\caption{ICA reduction to 7 components.}
		\label{fig:tsne:qsar:ica}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RP/qsar_tsne.pdf}
		\caption{RP reduction to 8 components.}
		\label{fig:tsne:qsar:rp}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RF/qsar_tsne.pdf}
		\caption{RF reduction to 4 components.}
		\label{fig:tsne:qsar:rf}
	\end{subfigure}
	\caption{{t-SNE} projections of ``QSAR'' data.}
	\label{fig:tsne:qsar}
\end{figure*}

T-distributed Stochastic Neighbor Embedding ({t-SNE}) is a popular algorithm to represent multidimensional data within a two-dimensional space in a way that tries to preserve the local structure. We can use {t-SNE} to visualize and compare the results against the original features.

Figure \ref{fig:tsne:qsar:benchmark} shows that the original QSAR data has a small bit of separation -- especially in the left side and upper-right area. PCA (figure \ref{fig:tsne:qsar:pca}) seems to be slightly worse than the original features, and ICA (figure \ref{fig:tsne:qsar:ica}) seems to be about equal, only switching the orderliness of the purple groupings over to yellow, and shifting yellow's chaos to purple; there appears to be no gain. Both RP and RF (figures \ref{fig:tsne:qsar:rp} and \ref{fig:tsne:qsar:rf}) seem to do the best job and are the closest we get to a linear separation.

\begin{figure*}[tbp]
	\centering
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/benchmark/wine_tsne.pdf}
		\caption{Original 11 features.}
		\label{fig:tsne:wine:benchmark}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/PCA/wine_tsne.pdf}
		\caption{PCA reduction to 3 components.}
		\label{fig:tsne:wine:pca}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/ICA/wine_tsne.pdf}
		\caption{ICA reduction to 3 components.}
		\label{fig:tsne:wine:ica}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RP/wine_tsne.pdf}
		\caption{RP reduction to 9 components.}
		\label{fig:tsne:wine:rp}
	\end{subfigure}
	\begin{subfigure}[t]{0.30\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RF/wine_tsne.pdf}
		\caption{RF reduction to 3 components.}
		\label{fig:tsne:wine:rf}
	\end{subfigure}
	\caption{{t-SNE} projections of ``wine'' data.}
	\label{fig:tsne:wine}
\end{figure*}

Figure \ref{fig:tsne:wine} shows that the wine data is more intermixed in its original state, but also shows a bit of order; purple is predominantly in the lower left and yellow in the upper right, but there are few distinct groupings. None of the algorithms seem to do much of anything to make this better, unfortunately, but it feels like ICA and RF are moderately more successful than the other algorithms.


\section{Clustering with Dimensionality-Reduced Features}\label{dr-clustering}

The techniques of clustering and dimensionality reduction can be combined together to sometimes find new ways to represent and classify a set of data.

\subsection{Methodology}\label{dr-clustering:methodology}

\begin{table}[htbp]
	\centering
	\begin{tabular}{lrr}
		\hline
		Algorithm & QSAR & Wine \\
		\hline
		PCA       & 6    & 3    \\
		ICA       & 7    & 3    \\
		RP        & 8    & 9    \\
		RF        & 4    & 3    \\
		\hline
	\end{tabular}
	\caption{Values used for dimensionality reduction prior to clustering.}
	\label{tbl:dr-clustering:dr-values}
\end{table}

For this experiment, the features of each data set were subjected to dimensionality reduction using the four algorithms discussed in section \ref{dim-reduction}. The number of dimensions used is summarized in table \ref{tbl:dr-clustering:dr-values} and discussed in section \ref{dim-reduction:analysis}. The results of each reduction were next subjected to both clustering algorithms using the methodology described in section \ref{clustering:methodology}.

\subsection{Analysis}\label{dr-clustering:analysis}

\subsubsection*{PCA}\label{dr-clustering:pca}

\begin{figure*}[tbp]
	\centering
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/PCA/qsar_GMM_combined.pdf}
		\caption{GMM clustering for ``QSAR'' PCA.}
		\label{fig:dr-clustering:pca:qsar:GMM_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/PCA/qsar_Kmeans_combined.pdf}
		\caption{$k$-means clustering for ``QSAR'' PCA.}
		\label{fig:dr-clustering:pca:qsar:kmeans_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/PCA/wine_GMM_combined.pdf}
		\caption{GMM clustering for ``wine'' PCA.}
		\label{fig:dr-clustering:pca:wine:GMM_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/PCA/wine_Kmeans_combined.pdf}
		\caption{$k$-means clustering for ``wine'' PCA.}
		\label{fig:dr-clustering:pca:wine:kmeans_combined}
	\end{subfigure}
	\caption{Combined clustering results for PCA components.}
	\label{fig:dr-clustering:pca:combined}
\end{figure*}

Figure \ref{fig:dr-clustering:pca:combined} shows the results for PCA clustering. In comparison with the original clustering experiment, the only obvious change is that some of the chaos seen with the smallest number of clusters is reduced, but the lines generally follow the same pattern.

Looking at $k$-means (figures \ref{fig:dr-clustering:pca:qsar:kmeans_combined} and \ref{fig:dr-clustering:pca:wine:kmeans_combined}) does show a notable increase in silhouette scores, indicating that the clusters of the dimensionality-reduced ``features'' were better separated as compared to the benchmark clustering.

It is also interesting to note that the GMM silhouette scores remained relatively unchanged. This is perhaps explained by how GMM assigns assignment probabilities rather than definitive assignments; GMM was able to recognize that the points still had similar relations to the various clusters.

Combining this together, and given that the accuracy results remained relatively unchanged, it appears that this technique worked to maintain the relationships of the data within the dimensional space.

\subsubsection*{ICA}\label{dr-clustering:ica}

\begin{figure*}[tbp]
	\centering
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/ICA/qsar_GMM_combined.pdf}
		\caption{GMM clustering for ``QSAR'' ICA.}
		\label{fig:dr-clustering:ica:qsar:GMM_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/ICA/qsar_Kmeans_combined.pdf}
		\caption{$k$-means clustering for ``QSAR'' ICA.}
		\label{fig:dr-clustering:ica:qsar:kmeans_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/ICA/wine_GMM_combined.pdf}
		\caption{GMM clustering for ``wine'' ICA.}
		\label{fig:dr-clustering:ica:wine:GMM_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/ICA/wine_Kmeans_combined.pdf}
		\caption{$k$-means clustering for ``wine'' ICA.}
		\label{fig:dr-clustering:ica:wine:kmeans_combined}
	\end{subfigure}
	\caption{Combined clustering results for ICA components.}
	\label{fig:dr-clustering:ica:combined}
\end{figure*}

The results of ICA (figure \ref{fig:dr-clustering:ica:combined}) show similar results as PCA. Lines are smoother, accuracy is generally slightly worse, and mean silhouette coefficients are noticeably higher for $k$-means.

Interestingly, figure \ref{fig:dr-clustering:ica:wine:GMM_combined} shows that GMM clustering on the wine data also produced significantly higher silhouette scores. As noted in the analysis of the {t-SNE} plots, ICA did seem to result in more defined clusters after performing ICA reduction, and this result seems to confirm that observation.

\subsubsection*{RP}\label{dr-clustering:rp}

\begin{figure*}[tbp]
	\centering
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RP/qsar_GMM_combined.pdf}
		\caption{GMM clustering for ``QSAR'' RP.}
		\label{fig:dr-clustering:rp:qsar:GMM_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RP/qsar_Kmeans_combined.pdf}
		\caption{$k$-means clustering for ``QSAR'' RP.}
		\label{fig:dr-clustering:rp:qsar:kmeans_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RP/wine_GMM_combined.pdf}
		\caption{GMM clustering for ``wine'' RP.}
		\label{fig:dr-clustering:rp:wine:GMM_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RP/wine_Kmeans_combined.pdf}
		\caption{$k$-means clustering for ``wine'' RP.}
		\label{fig:dr-clustering:rp:wine:kmeans_combined}
	\end{subfigure}
	\caption{Combined clustering results for RP components.}
	\label{fig:dr-clustering:rp:combined}
\end{figure*}

Figure \ref{fig:dr-clustering:rp:combined} shows the results of clustering the RP reductions. Again, we find that lines are smoother, especially with few clusters. Accuracy is relatively unchanged, and silhouette scores are slightly increased for $k$-means -- but GMM scores are not significantly better for the wine data, and more in line with the changes seen with PCA.

\subsubsection*{RF}\label{dr-clustering:rf}

\begin{figure*}[tbp]
	\centering
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RF/qsar_GMM_combined.pdf}
		\caption{GMM clustering for ``QSAR'' RF.}
		\label{fig:dr-clustering:rf:qsar:GMM_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RF/qsar_Kmeans_combined.pdf}
		\caption{$k$-means clustering for ``QSAR'' RF.}
		\label{fig:dr-clustering:rf:qsar:kmeans_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RF/wine_GMM_combined.pdf}
		\caption{GMM clustering for ``wine'' RF.}
		\label{fig:dr-clustering:rf:wine:GMM_combined}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/RF/wine_Kmeans_combined.pdf}
		\caption{$k$-means clustering for ``wine'' RF.}
		\label{fig:dr-clustering:rf:wine:kmeans_combined}
	\end{subfigure}
	\caption{Combined clustering results for RF components.}
	\label{fig:dr-clustering:rf:combined}
\end{figure*}

RF clustering is shown in figure \ref{fig:dr-clustering:rf:combined}, and the results are similar to ICA. $k$-means showed significantly better silhouette scores, as did the results of GMM on the reduced wine dimensions.

The QSAR data also had extremely high silhouette scores at the smallest cluster sizes (as seen in figures \ref{fig:dr-clustering:rf:qsar:GMM_combined} and \ref{fig:dr-clustering:rf:qsar:kmeans_combined}). This is also reflected in the AMI results, which uncharacteristically start very low at small clusters; however, the experiments have shown a generally inverse relationship between silhouette and AMI, and this further confirms that relationship. However, it is difficult to account for this dramatic shift -- especially considering that there were no noticeable changes to label accuracy or the BIC/SSE curves.


\section{Using Dimensionality-Reduced Features in a Neural Network}\label{nn-dr}

To further explore the dimensionality-reduction techniques studied so far, we can apply them to the neural network created for the same data in assignment 1.

\subsection{Methodology}\label{nn-dr:methodology}

The components chosen for each algorithm in section \ref{dim-reduction} were used as features to train an artificial neural network learner. The learner was created using the same parameters selected in assignment 1 as optimal for each data set. The learner performed the same type of training as before on each algorithm's data, utilizing five-fold cross-validation to train the learner.

As a benchmark, the original features of the data set were also used to train the same learner.

The results of this process are shown in table \ref{tbl:nn-dr}.

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table*}[tbp]
	\centering
	\begin{tabular}{llrrrrrrrr}
		\hline
		\multirow{2}{*}{Data Set} 	& \multirow{2}{*}{Algorithm} 	& \multicolumn{3}{c}{Mean time} 			& \multicolumn{2}{c}{Mean score} 	& \multicolumn{3}{c}{Comparison to benchmark} 	\\
									& 								& Fit 		& Score 	& Total 	& Train 		& Test 			& Time 		& Test score 	& Improvement 		\\
		\hline
		QSAR 						& Benchmark 					& 0.4547 	& 0.002663 	& 0.457 	& 0.904 		& 0.838 		& \textcolor{gray}{100\%} & \textcolor{gray}{100\%} & \textcolor{gray}{0\%} \\
		QSAR 						& PCA 							& 1.6195 	& 0.000852 	& 1.620 	& 0.843 		& 0.816 		& 354\% 	& 	 97\% 	& 	 27\% 				\\
		QSAR 						& ICA 							& 0.4681 	& 0.001173 	& 0.469 	& 0.629 		& 0.619 		& 103\% 	& 	 74\% 	& 	 72\% 				\\
		QSAR 						& RP 							& 0.3661 	& 0.001066 	& 0.367 	& 0.877 		& 0.855 		&  80\% 	& 	102\% 	& 	127\% 				\\
		QSAR 						& RF 							& 0.6093 	& 0.227091 	& 0.836 	& 0.737 		& 0.729 		& 183\% 	& 	 87\% 	& 	 48\% 				\\
		\hline
		Wine 						& Benchmark 					& 0.1035 	& 0.000687 	& 0.104 	& 0.738 		& 0.723 		& \textcolor{gray}{100\%} & \textcolor{gray}{100\%} & \textcolor{gray}{0\%} \\
		Wine 						& PCA 							& 0.1263 	& 0.000644 	& 0.127 	& 0.699 		& 0.711 		& 122\% 	& 	 98\% 	& 	 81\% 				\\
		Wine 						& ICA 							& 0.0786 	& 0.000661 	& 0.079 	& 0.500 		& 0.500 		&  76\% 	& 	 69\% 	& 	 91\% 				\\
		Wine 						& RP 							& 0.0848 	& 0.000843 	& 0.086 	& 0.709 		& 0.701 		&  82\% 	& 	 97\% 	& 	118\% 				\\
		Wine 						& RF 							& 0.3454 	& 0.103606 	& 0.449 	& 0.730 		& 0.723 		& 431\% 	& 	100\% 	& 	 23\% 				\\
		\hline
	\end{tabular}
	\caption[Cross-validation results]{Cross-validation results from neural-network experiments on dimensionality-reduced data.}
	\label{tbl:nn-dr}
\end{table*}

In order to compare the performance of the algorithms against each other and the benchmark, the final columns of table \ref{tbl:nn-dr} (labeled as ``comparison to benchmark'') are useful. These are calculated as ratios of the given measure against the same measure for the benchmark obtained on the original data. The ratios are expressed as percentages.

The ``improvement'' percentage shown in the table is calculated by dividing the score ratio by that of the time. The intent of this is to measure the change in score in relation to the change in time spent obtaining that score change. As an example, if the score was half as good as the benchmark but took half the time to train and test, the improvement measure would be 0\% since there is no improvement. However, if the score was the same and took half as long, that would be 100\% improvement.

\subsection{Analysis}\label{nn-dr:analysis}

\subsubsection*{QSAR}\label{nn-dr:analysis:qsar}

Focusing first on the QSAR data, PCA and RP are the only two algorithms that were able to maintain accuracy. But unlike RP, PCA took 3.5 times longer than the benchmark -- far and away the longest time, making it the clear loser for this data.

\subsubsection*{Wine}\label{nn-dr:analysis:wine}

Turning to the wine data, there are three algorithms that maintain accuracy in the scoring, with only ICA performing poorly in that regard. However, looking at the time needed to train the learners shows that the RF-reduced data took more than four times longer than training on the original features. That time resulted in test scores of equal accuracy, but because it took so long it is the clear loser at this data set.

\subsubsection*{Conclusion}\label{nn-dr:analysis:conclusion}

Using the ``improvement'' measure described above, there is a clear winner: randomized projection. It trains quickly (taking approximately 80\% of the time as the benchmark learner) while staying within a few percentage points of the benchmark score.


\section{Using Clusters as Features in a Neural Network}\label{nn-clustering}

Finally, to further explore the clustering techniques studied in this assignment, we can use the same technique used in section \ref{nn-dr} -- use the clusters as inputs to the same neural network learner.

\subsection{Methodology}\label{nn-clustering:methodology}

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table}[htbp]
	\centering
	\begin{tabular}{llll}
		\hline
		Hyperparameter 					& Values searched 									\\
		\hline
		Components/clusters 			& 2, 3, \textellipsis 10, 15, 20, \textellipsis 40 	\\
		\multirow{2}{*}{Hidden layers} 	& $25$, $50$, $25 \times 25$, 						\\[-1pt]
		& $50\times50$, $100 \times 25 \times 100$ 			\\
		Alpha 							& $10^{-5}$ through $10^{-1}$ 						\\
		\hline
	\end{tabular}
	\caption{Parameters used to search for optimal neural networks for each algorithm. Components and clusters were searched for the clustering algorithms only.}
	\label{tbl:nn-clustering:gridsearch:parameters}
\end{table}

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table}[htbp]
\centering
\begin{tabular}{lllcc}
	\hline
	Data set              	& Algorithm 	& Alpha 	& Hidden layers 				& Features 	\\
	\hline
	\multirow{3}{*}{QSAR} 	& Benchmark 	& 0.01 		& $100 \times 25 \times 100$ 	& 41 		\\
							& GMM 			& 0.01 		& $50$ 							& 25 		\\
							& $k$-means 	& 0.1 		& $100 \times 25 \times 100$ 	& 25 		\\
	\hline
	\multirow{3}{*}{Wine} 	& Benchmark 	& 0.001 	& $100 \times 25 \times 100$ 	& 11 		\\
							& GMM 			& 0.001 	& $50 \times 50$ 				& 15 		\\
							& $k$-means 	& 0.0001 	& $50 \times 50$ 				&  9 		\\
	\hline
\end{tabular}
\caption{Results of grid search for best parameters for clustered data. (``Features'' is components for GMM, and clusters for $k$-means.)}
\label{tbl:nn-clustering:gridsearch:results}
\end{table}

As it is a normal part of creating a neural network learner, a grid search was performed on each data set to find the optimal learner parameters. For the clustering algorithms, the search included a range of cluster values to find their optimal values. The parameters searched are listed in table \ref{tbl:nn-clustering:gridsearch:parameters}, and optimal values found are shown in table \ref{tbl:nn-clustering:gridsearch:results}.

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table*}[htbp]
	\centering
	\begin{tabular}{llrrrrrrrr}
		\hline
		\multirow{2}{*}{Data set} & \multirow{2}{*}{Algorithm} & \multicolumn{3}{c}{Mean time (ms)} & \multicolumn{2}{c}{Mean score} & \multicolumn{3}{c}{Comparison to benchmark} \\
		&                            & Fit        & Score     & Total     & Test           & Train         & Total time   & Test score   & Improvement   \\
		\hline
		QSAR                      & Benchmark                  & 3950       & 58        & 4007      & 0.8312         & 0.8740        & 100\%        & 100\%        & 0\%           \\
		QSAR                      & GMM                        & 11738      & 122       & 11860     & 0.7145         & 0.7069        & 296\%        & 86\%         & -71\%         \\
		QSAR                      & k-means                    & 5720       & 9         & 5729      & 0.7767         & 0.7827        & 143\%        & 93\%         & -35\%         \\
		\hline
		Wine                      & Benchmark                  & 2643       & 20        & 2663      & 0.7280         & 0.7479        & 100\%        & 100\%        & 0\%           \\
		Wine                      & GMM                        & 4852       & 36        & 4888      & 0.6894         & 0.6713        & 184\%        & 95\%         & -48\%         \\
		Wine                      & k-means                    & 4362       & 19        & 4381      & 0.7307         & 0.7331        & 165\%        & 100\%        & -39\%        \\
		\hline
	\end{tabular}
	\caption{Cross-validation results from neural-network experiments on clustered data.}
	\label{tbl:nn-clustering}
\end{table*}

Again, five-fold cross-validation was performed with the learner to train and fit the data. The results are listed in table \ref{tbl:nn-clustering}.

\subsection{Analysis}\label{nn-clustering:analysis}

The results show that the clustering algorithms all performed either significantly slower or with a significant decrease in testing accuracy.

The GMM clustering did not perform well on the QSAR data. It took three times as long and had the lowest accuracy ratio. Why the learner took so long is unclear; it had the same number of features as $k$-means and a much smaller network of hidden layers. However, we also see that GMM performed poorly on the wine data. This suggests that the algorithm itself may cluster the data in a manner which does not work very well with this type of learner. However, it could also indicate that GMM does not do well with clustering numeric feature spaces.

The results for $k$-means was slightly better than GMM, but still performed poorly overall. Its testing accuracy on the wine data set was equal to that of learning against the original data, but it took 65\% longer to do so.

In conclusion, clustering did not work well with these data sets nor showed any improvements.


% use section* for acknowledgment
\ifCLASSOPTIONcompsoc
% The Computer Society usually uses the plural form
\section*{Acknowledgments}
\else
% regular IEEE prefers the singular form
\section*{Acknowledgments}
\fi

The majority of the code used for this project was produced by Chad Maron\cite{cmaron-code} and Jonathan Tay\cite{jontay-code}; many thanks to them both.

Also thanks to the maintainers of the ``scikit-learn'' library\cite{scikit-learn}.

\bibliography{mhobson7-analysis}{}
\bibliographystyle{plain}

\end{document}
