# Project 3: Unsupervised Learning

Matt Hobson  
CS7641 - Spring 2019  
GTID: mhobson7

## Code

Code for this assignment can be found here:

    https://gitlab.com/mhobson/cs7641-assignment3

This was developed under Python 3.7. You can probably run it with 3.5 or later.

If you are familiar with Pipenv, you can use that to set up your virtual environment. If not, there's a standard requirements file you can use.

This code requires [HDF5 libraries](https://www.hdfgroup.org/downloads/hdf5/) to be installed.

## Data sets

The data files are included in the repository. If you prefer to download the files yourself, there are links to the sourcce UCI pages in the README files within each data directory.

## Running the project

1. Run `python run_experiment.py --threads -1 --all` to run the initial experiments. This performs the initial clustering analysis (part 1) as well as applying the clustering to the neural network (part 5).
2. Run `run_clustering.sh` to run the four algorithms. This performs the dimensionality-reduction experiments (part 3) and applies the reduced dimensions to the neural network (part 4).
   - The values I used for optimal dimensions are already configured. If you wish to alter them, update the values in the script prior to running it.
3. Plot the final results via `python run_experiment.py --plot`.

## Acknowledgements

The base of the code used for this project is from [Chad Maron](https://github.com/cmaron/CS-7641-assignments) and [Jonathan Tay](https://github.com/JonathanTay/CS-7641-assignment-3).
