#!/bin/sh

# Replace 'X' below with the optimal values found
# If you want to first generate data and updated datasets, remove the "--skiprerun" flags below

NUM_THREADS="-1"

# ICA
python run_experiment.py --verbose --threads "${NUM_THREADS}" \
  --ica --dataset1 --dim 7 > ica-dataset1-clustering.log 2>&1
python run_experiment.py --verbose --threads "${NUM_THREADS}" \
  --ica --dataset2 --dim 3 > ica-dataset2-clustering.log 2>&1

# PCA
python run_experiment.py --verbose --threads "${NUM_THREADS}" \
  --pca --dataset1 --dim 6 > pca-dataset1-clustering.log 2>&1
python run_experiment.py --verbose --threads "${NUM_THREADS}" \
  --pca --dataset2 --dim 3 > pca-dataset2-clustering.log 2>&1

# RP
python run_experiment.py --verbose --threads "${NUM_THREADS}" \
  --rp  --dataset1 --dim 8 > rp-dataset1-clustering.log 2>&1
python run_experiment.py --verbose --threads "${NUM_THREADS}" \
  --rp  --dataset2 --dim 9 > rp-dataset2-clustering.log 2>&1

# RF
python run_experiment.py --verbose --threads "${NUM_THREADS}" \
  --rf  --dataset1 --dim 4 > rf-dataset1-clustering.log 2>&1
python run_experiment.py --verbose --threads "${NUM_THREADS}" \
  --rf  --dataset2 --dim 3 > rf-dataset2-clustering.log 2>&1

#python run_experiment.py --svd --dataset1 --dim X --skiprerun --verbose --threads -1 > svd-dataset1-clustering.log 2>&1
#python run_experiment.py --svd --dataset2 --dim X --skiprerun --verbose --threads -1 > svd-dataset2-clustering.log 2>&1
